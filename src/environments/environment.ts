// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyDtKq2lzVosRsqznyqYqf4ZL0CFAYIZ81g',
    authDomain: 'ingreso-egreso-app-df5ba.firebaseapp.com',
    databaseURL: 'https://ingreso-egreso-app-df5ba.firebaseio.com',
    projectId: 'ingreso-egreso-app-df5ba',
    storageBucket: 'ingreso-egreso-app-df5ba.appspot.com',
    messagingSenderId: '246429259368',
    appId: '1:246429259368:web:4ff56fb66e1694837f8ddd'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
