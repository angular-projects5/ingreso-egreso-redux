import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppStateWithIngreso } from '../ingreso-egreso.reducer';

import { IngresoEgreso } from 'src/app/models/ingreso-egreso.model';
import { Subscription } from 'rxjs';

import { MultiDataSet, Label } from 'ng2-charts';

@Component({
  selector: 'app-estadistica',
  templateUrl: './estadistica.component.html',
  styles: []
})
export class EstadisticaComponent implements OnInit, OnDestroy {

  ingresos = 0;
  egresos = 0;

  totalEgresos = 0;
  totalIngresos = 0;

  ingresoEgresoSubs: Subscription;

  // Doughnut
  public doughnutChartLabels: Label[] = ['Ingresos', 'Egresos'];
  public doughnutChartData: MultiDataSet = [[]];

  constructor(private store: Store<AppStateWithIngreso>) { }

  ngOnInit(): void {
    this.ingresoEgresoSubs = this.store.select('ingresosEgresos').subscribe(({ items }) => this.generarEstadistica(items))
  }

  ngOnDestroy() {
    this.ingresoEgresoSubs.unsubscribe();
  }

  generarEstadistica(items: IngresoEgreso[]) {
    console.log(items);
    const ingresos = items.filter(item => item.tipo === 'ingreso');
    const egresos = items.filter(item => item.tipo === 'egreso');

    this.ingresos = ingresos.length;
    this.egresos = egresos.length;

    this.totalIngresos = ingresos.reduce((acumulador, elemento) => acumulador + parseInt(elemento.monto, 10), 0);
    this.totalEgresos = egresos.reduce((acumulador, elemento) => acumulador + parseInt(elemento.monto, 10), 0);

    this.doughnutChartData = [[this.totalIngresos, this.totalEgresos]];

  }

}
