import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { dashboardRoutes } from './dashboard.routes';

const rutasHijas: Routes = [
  {
    path: '',
    component: DashboardComponent,
    children: dashboardRoutes,
    // con lazy load no funciona el canActivate, es el canLoad para saber si lo carga con antelación
    // canActivate: [AuthGuard]
  },
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(rutasHijas)
  ],
  //Se exporta el router module para decirle a angular que cuente también con estas rutas hijas
  exports: [
    RouterModule
  ]
})
export class DashboardRoutesModule { }
