import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.reducer';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styles: []
})
export class SidebarComponent implements OnInit, OnDestroy {
  nombre: string;
  userSubs: Subscription;

  constructor(private auth: AuthService,
    private router: Router,
    private store: Store<AppState>) { }

  ngOnInit(): void {
    this.userSubs = this.store.select('user')
      .pipe(filter(({ user }) => user != null))
      .subscribe(({ user }) => this.nombre = user.nombre)
  }

  ngOnDestroy() {
    this.userSubs.unsubscribe();
  }

  async logout() {
    this.auth.logout()
      .then((logout) => {
        console.log(logout);
        this.router.navigate(['/login']);

      });
  }

}
